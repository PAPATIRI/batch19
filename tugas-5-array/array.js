//soal no 1 range
function range(startNum, finishNum) {
  var temp = [];
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i++) {
      temp.push(i);
    }
  } else if (startNum >= finishNum) {
    for (var i = startNum; i >= finishNum; i--) {
      temp.push(i);
    }
  } else {
    temp = -1;
  }
  return temp;
}
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

//soal no 2 range with step
function rangeWithStep(startNum, finishNum, step) {
  var temp = [];
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i += step) {
      temp.push(i);
    }
  } else if (startNum >= finishNum) {
    for (var i = startNum; i >= finishNum; i -= step) {
      temp.push(i);
    }
  }
  return temp;
}
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

//soal no 3 sum of range
function sum(startNum, finishNum, step = 1) {
  var results = 0;
  if (
    (startNum < finishNum && step != undefined) ||
    (startNum < finishNum && step == undefined)
  ) {
    for (var i = startNum; i <= finishNum; i += step) {
      results += i;
    }
  } else if (
    (startNum > finishNum && step != undefined) ||
    (startNum > finishNum && step == undefined)
  ) {
    for (var i = startNum; i >= finishNum; i -= step) {
      results += i;
    }
  } else if (startNum != undefined && finishNum == undefined) {
    results += startNum;
  } else {
    results = results;
  }
  return results;
}
console.log(sum(1, 10)); // 55z
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

//soal no 4 array multidimensi
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

var results = "";
function dataHandling(input) {
  for (var i = 0; i < input.length; i++) {
    for (var j = 0; j < input[i].length; j++) {
      results.concat(console.log(input[i][j]));
    }
  }
  return results;
}
console.log(dataHandling(input));

//soal no 5 balik kata
function balikKata(str) {
  var balik = "";
  for (var i = str.length - 1; i >= 0; i--) {
    balik += str[i];
  }
  return balik;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

//soal no 6 method array
var input2 = [
  "0001",
  "Roman Alamsyah",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];
//["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
function dataHandling2() {
  input2.splice(1, 2, "Roman Alamsyah Elshawary", "Provinsi Bandar Lampung");
  input2.splice(4, 1, "Pria", "SMA Internasional Metro");

  var date = input2[3].split("/");
  // console.log(date);
  switch (date[1]) {
    case "01":
      date.splice(1, 1, "Januari");
      break;
    case "02":
      date.splice(1, 1, "Februari");
      break;
    case "03":
      date.splice(1, 1, "Maret");
      break;
    case "04":
      date.splice(1, 1, "April");
      break;
    case "05":
      date.splice(1, 1, "Mei");
      break;
    case "06":
      date.splice(1, 1, "Juni");
      break;
    case "07":
      date.splice(1, 1, "Juli");
      break;
    case "08":
      date.splice(1, 1, "Agustus");
      break;
    case "09":
      date.splice(1, 1, "September");
      break;
    case "10":
      date.splice(1, 1, "Oktober");
      break;
    case "11":
      date.splice(1, 1, "November");
      break;
    case "12":
      date.splice(1, 1, "Desember");
      break;
  }
  console.log(date[1]); //hasil ubah angka bulan ke string

  console.log(
    date.sort(function (a, b) {
      return b - a;
    })
  ); //hasil sorting

  console.log(date.join("-")); // join tanggal dengan -

  var namaLimit = input2[1].slice(0, 14); //pembatasan 15 karakter
  console.log(namaLimit);
  return input2;
}
console.log(dataHandling2(input2));
