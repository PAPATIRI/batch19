//todo SOAL NO 1
//looping pertama
var nomor = 2;
while (nomor <= 20) {
  console.log(nomor + " - I love coding");
  nomor += 2;
}
console.log(" ");
//looping ke dua
var nomor2 = 20;
while (nomor2 >= 2) {
  console.log(nomor2 + " - I will become a mobile developer");
  nomor2 -= 2;
}
console.log(" ");

//todo SOAL NO 2
for (var no = 1; no <= 20; no++) {
  if (no % 2 != 0) {
    if (no % 3 == 0) {
      console.log(no + " - I love coding");
    } else {
      console.log(no + " - santai");
    }
  } else if (no % 2 == 0) {
    console.log(no + " - Berkualitas");
  }
}
console.log(" ");

//todo SOAL NO 3
for (var i = 1; i <= 4; i++) {
  console.log("#".repeat(8));
}
console.log(" ");

//todo SOAL NO 4
var batas = 7;
var count = 0;
while (batas) {
  count++;
  var tangga = "#".repeat(count);
  console.log(tangga);
  batas--;
}
console.log(" ");

//todo SOAL NO 5
var kolom = 4;
for (var i = 1; i <= 4; i++) {
  console.log(" #".repeat(kolom));
  console.log("# ".repeat(kolom));
}
