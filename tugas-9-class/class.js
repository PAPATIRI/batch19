//todo 1. animal class
//release 0
class Animal {
  constructor(name, legs = 4, cold_blodded = false) {
    this._legs = legs;
    this._name = name;
    this._cold_blodded = cold_blodded;
  }
  get legs() {
    return this._legs;
  }
  set legs(x) {
    this._legs = x;
  }

  get cold_blodded() {
    return this._cold_blodded;
  }
  set cold_blodded(x) {
    this._cold_blodded = x;
  }

  get name() {
    return this._name;
  }
  set name(x) {
    this._name = x;
  }
}

let sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

//release 1
class Ape extends Animal {
  constructor(legs = 2) {
    super();
    this._legs = legs;
  }

  yell() {
    return "Auooo";
  }
}

class Frog extends Animal {
  constructor(legs) {
    super(legs);
  }

  jump() {
    return "hop hop";
  }
}

let sungokong = new Ape("kera sakti");
console.log(sungokong.yell()); // "Auooo"

var kodok = new Frog("buduk");
console.log(kodok.jump()); // "hop hop"

//todo 2 function  to class
class Clock {
  constructor({ template }, timer) {
    this._template;
    this._timer;
  }

  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    let output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }
  stop() {
    this.stop(clearInterval(this._timer));
  }
  start() {
    this.render();
    this._timer = this.start(setInterval(render, 1000));
  }
}

let clock = new Clock({ template: "h:m:s" });
clock.start();
