// tugas no 1
function teriak() {
  return "Halo Sanbers!";
}
console.log(teriak());

// tugas no 2
function kalikan(angka1, angka2) {
  return angka1 * angka2;
}
var no1 = 12;
var no2 = 4;
var hasilKali = kalikan(no1, no2);
console.log(hasilKali);

// tugas no 3
function introduce(name, age, address, hobby) {
  var results =
    "nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    address +
    ", dan saya punya hobby yaitu " +
    hobby;
  return results;
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"
