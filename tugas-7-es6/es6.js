//todo ubah fungsi  biasa ke fungsi arrow
const golden = () => {
  console.log("this is golden");
};
golden();

//todo menyederhanakan dengan objek literal
const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      return console.log(`${firstName} ${lastName}`);
    },
  };
};
newFunction("william", "imoh").fullName();

//todo  destructuring object
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);

//todo array spreading
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east);
const combined = [...west, ...east];
console.log(combined);

//todo template literals
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet,  
    consectetur adipiscing elit ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`;

console.log(before);
